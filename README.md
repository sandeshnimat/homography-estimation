## Homography Estimation
------------------------------------------------------------------------------------
<!-- will add information as development progresses -->
Homography calculation is important for many computer vision applications such as image registration, image stitching, image rectification etc., to establish relation between two planar images. Traditional feature based and direct methods for calaculating homographies are slow and might even fail when theres lack of matching features. This problem can be solved by training a neural net to identify homographies between pairs of images.  

This is tensorflow keras based implementation of homography estimation using deep convolutional neural networks. Two types of deep CNN architectures were used :
1. Deep Regression Net
2. Residual Network

The implementation is based on the paper [Deep Image Homography Estimation](https://arxiv.org/abs/1606.03798) and [Deep Residual Learning for Image Recognition](https://arxiv.org/abs/1512.03385).

--------------------------------------------------------------------------------------

### Data generation

Synthetic data is generated using MS COCO dataset images. 

Python COCO API is used to access COCO images and then preprocessed to build synthetic dataset. 

![image](images/coco.png)

Make use of Data Generation and Preprocessing.ipynb to generate synthetic data.  

Specify annotations file to access COCO images and save path to save data in hdf5 format.

![image](images/save.png)

--------------------------------------------------------------------------------------

### Training and Testing

ResNet Homography and Deep Homography Net.ipynb has the source code to train the models.

Specify the training and validation data path, as well as model saving path to train the model.

![image](images/trainpath.png)

![image](images/modeltrain.png)


Model Evaluation.ipynb evaluates the model on test data specified by test data path.

![image](images/model_eval.png)

-------------------------------------------------------------------------------------

### Results :
Black - Predicted, 
White - Ground Truth

1. Deep Homography Net

![image](images/deepnet1.png)
![image](images/deepnet2.png)

1. ResNet Homography

![image](images/resnet1.png)
![image](images/resnet2.png)

--------------------------------------------------------------------------------------

If unable to open IPython notebook, copy and paste the notebook's link [here](https://nbviewer.jupyter.org/).